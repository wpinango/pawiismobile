import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:pawiis/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

ValueNotifier<User> currentUser = new ValueNotifier(User());

Future<User> login(User user) async {
  final String url = MyApp.url + 'api/user/login';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    },
    body: json.encode(user),
  ).timeout(Duration(seconds: 40));
  if (response.statusCode == 200) {
    var data = jsonDecode(response.body);
    if (data['message']) {
      currentUser.value = User.fromJson(data['data']);
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString('user', json.encode(currentUser.value));
      sharedPreferences.setBool('login', true);
    } else {

    }
  } else {
    throw new Exception(response.body);
  }
  return currentUser.value;
}

Future<User> findUserById(String userId) async {
  User user;
  final String url = MyApp.url + '/api/user/$userId';
  final client = new http.Client();
  final response = await client.get(
    url,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
    },
  ).timeout(Duration(seconds: 40));
  if (response.statusCode == 200) {
    var data = jsonDecode(response.body);
    if (data['message']) {
      user = User.fromJson(data['data']);
    } else {

    }
  } else {
    throw new Exception(response.body);
  }
  return user;
}