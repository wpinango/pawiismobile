import 'dart:convert';
import 'dart:io';

import 'package:pawiis/models/pet.dart';
import 'package:http/http.dart' as http;
import 'package:pawiis/repositories/user_repositoy.dart';

import '../main.dart';

Future<dynamic> getPetsByUserId(int userId) async {
  final String completeUrl = MyApp.url + 'api/mascotas/list/$userId';
  final client = new http.Client();
  String token = currentUser.value.token;
  final response = await client.get(
    completeUrl,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ' + token
    },
  ).timeout(Duration(seconds: 120));
  var list;
  if (response.statusCode == 200) {
    print(response.body);
    var data = jsonDecode(response.body);
    if (data['message']) {
      list = data['data'];
    }
  } else {
    throw new Exception(response.body);
  }
  return list;
}

Future<dynamic> registerPet(Pet pet) async {
  final String completeUrl = MyApp.url + 'api/mascota';
  final client = new http.Client();
  String token = currentUser.value.token;
  final response = await client
      .post(
        completeUrl,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + token
        },
        body: jsonEncode(pet),
      )
      .timeout(Duration(seconds: 80));
  var dataResponse;
  if (response.statusCode == 200) {
    print(response.body);
    var data = jsonDecode(response.body);
    if (data['message']) {
      dataResponse = data['data'];
    }
  } else {
    throw new Exception(response.body);
  }
  return dataResponse;
}

Future<dynamic> deletePet(Pet pet) async {
  int id = pet.id;
  final String completeUrl = MyApp.url + 'api/mascota/$id';
  final client = new http.Client();
  String token = currentUser.value.token;
  final response = await client.delete(
    completeUrl,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ' + token
    },
  ).timeout(Duration(seconds: 80));
  var dataResponse;
  if (response.statusCode == 200) {
    print(response.body);
    var data = jsonDecode(response.body);
    if (data['message']) {
      dataResponse = data['data'];
    }
  } else {
    throw new Exception(response.body);
  }
  return dataResponse;
}

Future<dynamic> updatePet(Pet pet) async {
  int id = pet.id;
  final String completeUrl = MyApp.url + 'api/mascota/$id';
  final client = new http.Client();
  String token = currentUser.value.token;
  final response = await client.patch(
    completeUrl,
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: 'Bearer ' + token
    },
    body: jsonEncode(pet),
  ).timeout(Duration(seconds: 80));
  var dataResponse;
  if (response.statusCode == 200) {
    print(response.body);
    var data = jsonDecode(response.body);
    if (data['message']) {
      dataResponse = data['data'];
    }
  } else {
    throw new Exception(response.body);
  }
  return dataResponse;
}
