import 'package:intl/intl.dart';

class Validations{
  String validateName(String value) {
    if (value.isEmpty) return 'Es requerido este campo.';
    final RegExp nameExp = new RegExp(r'^[A-za-z ]+$');
    if (!nameExp.hasMatch(value))
      return 'Por favor introduzca caracteres alfabeticos.';
    return null;
  }

  String validateLastName(String value) {
    if (value.isEmpty) return 'Por favor introduzca un dato';
    return null;
  }

  String validateDate(String value) {
    if (value.isEmpty) return 'Debe introducir una fecha.';

  }

  String validateEmail(String value) {
    if (value.isEmpty) return 'El Correo es requerido.';
    //final RegExp nameExp = new RegExp(r'^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$');
    //if (!nameExp.hasMatch(value)) return 'Direccion de correo invalido';
    return null;
  }

  String validatePassword(String value) {
    if (value.isEmpty) return 'Por favor introduzca una contraseña valida.';
    if (value.length < 6) return 'Por favor introduzca 6 caracteres o mas';
    //RegExp exp = new RegExp(r'^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*-.])[\w!@#\$%^&*-.]{8,}$');
    //if (!exp.hasMatch(value)) return 'debe contener una mayuscula, una minuscula, numeros y caracteres especiales';
    return null;
  }

  String validateRepeatPassword(String value, String value2) {
    if (value == null || value.isEmpty) return 'Por favor introduzca una contraseña';
    //RegExp exp = new RegExp(r'^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*-.])[\w!@#\$%^&*-.]{8,}$');
    //if (!exp.hasMatch(value)) return 'debe contener una mayuscula, una minuscula, numeros y caracteres especiales';
    if (value != value2) return 'Las contraseña no coinciden';
    return null;
  }

  String validateTextInput(String value) {
    if (value == null) return 'Por favor introduzca un dato';
    if (value.isEmpty) return 'Por favor introduzca un dato';
    return null;
  }

  String validatePhone(String value) {
    if (value.isEmpty) return 'Por favor introduzca un numero';
    //RegExp exp = new RegExp(r'\+[0-9]+');
    //if (!exp.hasMatch(value)) return 'Introduzca codigo del pais';
    return null;
  }

  String validateAmountInput(String value) {
    if (value.isEmpty) return 'Por favor introduzca un monto';
    return null;
  }

  String validateLength(String value) {
    if (value.length < 2 || value.length > 45) return 'Debe introducir mas de 2 caracteres y maximo 45';
    return null;
  }

  String validateAddressLength(String value) {
    if (value.length < 20 || value.length > 255) return 'Debe introducir al menos 20 caracteres y maximo 255';
    return null;
  }

  String validateDniLength(String value) {
    if (value.length < 6 || value.length > 25) return 'Debe introducir al menos 6 caracteres y maximo 25';
    return null;
  }

  String validateSpecialPass(String value) {
    if (value.length != 8) return 'Debe introducir 8 caracteres';
    return null;
  }

  String validateDateIsBefore(String date) {
    if (date == null || date == '') return 'Debe ingresar una fecha';
    if (DateFormat('dd/MM/yyyy').parse(date).isBefore(new DateTime.now())) {
      return 'Debe agregar una fecha mayor a la actual';
    }
    return null;
  }
}