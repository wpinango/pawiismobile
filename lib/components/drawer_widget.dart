import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
class DrawerWidget extends StatefulWidget {
  final List<Widget> drawerItems;

  const DrawerWidget({Key key, this.drawerItems}) : super(key: key);

  @override
  _DrawerWidgetState createState() =>
      _DrawerWidgetState(drawerItems: drawerItems);
}

class _DrawerWidgetState extends StateMVC<DrawerWidget> {
  List<Widget> drawerItems;

  _DrawerWidgetState({this.drawerItems});

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: drawerItems,
      ),
    );
  }
}
