import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:pawiis/models/pet.dart';
import 'package:pawiis/models/route_argument.dart';

class PetItemWidget extends StatefulWidget {
  final Pet pet;

  const PetItemWidget({Key key, this.pet}) : super(key: key);

  @override
  _PetItemWidgetState createState() => _PetItemWidgetState(pet: pet);
}

class _PetItemWidgetState extends StateMVC<PetItemWidget> {
  bool loading = true;
  Pet pet;

  _PetItemWidgetState({this.pet});

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/PetInfo',
            arguments: RouteArgument(
                param: pet,
            ));
      },
      child: Container(
        margin: EdgeInsets.all(8),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  radius: 35,
                    backgroundImage: pet.avatarPath != null ?  NetworkImage(pet.avatarPath) : null
                ),
                SizedBox(
                  width: 8,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      pet.petName,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(pet.petType),
                    SizedBox(
                      height: 4,
                    ),
                    Text('Cliente: ' + pet.ownerName),
                    SizedBox(
                      height: 4,
                    ),
                    Text('Numero de registro: ' + pet.id.toString())
                  ],
                )
              ],
            ),
            Divider()
          ],
        ),
      ),
    );
  }
}
