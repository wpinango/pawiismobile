import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pawiis/controllers/base_controller.dart';
import 'package:pawiis/dialogs/dialog.dart';
import 'package:pawiis/models/pet.dart';
import 'package:pawiis/repositories/pet_repository.dart';
import 'package:pawiis/repositories/user_repositoy.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';


class HomeController extends BaseController{
  List<Widget> drawerItems;
  List<Pet> pets;

  HomeController() {
    pets = new List();
    drawerItems = new List();
    drawerItems.add(ListTile(
      title: Text("Cerrar sesion"),
      leading: Icon(Icons.exit_to_app),
      onTap: () async {
        if (await MyDialog.showNativePopUpWith2Buttons(context, 'Cerrar sesión',
            '¿Desea cerrar sesión?', 'Cancelar', 'Si'))  {
          //user = null;
          //currentUser.value.auth = null;
          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          sharedPreferences.setBool("login", false);
          Navigator.pushReplacementNamed(context, '/Login');
        }
      },
    ));
  }

  getMyPets() {
    print(currentUser.value);
    showProgressDialog();
    getPetsByUserId(currentUser.value.id).then((list) {
      setState((){
        pets.clear();
      });
      cancelProgressDialog();
      for (var item in list) {
        Pet pet = new Pet();
        pet.id = item['id'];
        pet.petName = item['pet_name'];
        pet.petOwnerId = item['pet_owner_id'];
        pet.petType = item['pet_type'];
        pet.ownerName = item['pet_owner_name'];
        pet.avatarPath = item['pet_image'];
        setState((){
          pets.add(pet);
        });
        imageCache.clear();
      }
    }).catchError((onError) {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    });
  }
}