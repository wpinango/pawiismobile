import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class BaseController extends ControllerMVC {
  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  bool isInAsyncCall;
  bool autoValidate = false;

  BaseController() {
    formKey = new GlobalKey<FormState>();
    scaffoldKey = new GlobalKey<ScaffoldState>();
    isInAsyncCall = false;
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  Future<bool> willPopCallback() async {
    Navigator.pop(context, false);
    return true;
  }
}