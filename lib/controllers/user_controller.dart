import 'package:flutter/cupertino.dart';
import 'package:pawiis/controllers/base_controller.dart';
import 'package:pawiis/models/user.dart';
import 'package:pawiis/repositories/user_repositoy.dart';
import 'package:pawiis/services/validations.dart';

class UserController extends BaseController {
  Validations validations;
  bool hidePass;
  User loginUser;

  UserController() {
    validations = new Validations();
    hidePass = true;
    loginUser = new User();
  }

  void handleFormSubmitted() {
    showProgressDialog();
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true;
      cancelProgressDialog();
      showInSnackBar('Por favor corrija los errores primero');
    } else {
      form.save();
      _doLogin();
    }
  }

  _doLogin() {
    showProgressDialog();
    login(loginUser).then((value) {
      cancelProgressDialog();
      showInSnackBar('Login correcto!');
      Navigator.pushReplacementNamed(context, '/Home');
    }).catchError((onError) {
      cancelProgressDialog();
      showInSnackBar('Credenciales invalidas');
    });
  }
}