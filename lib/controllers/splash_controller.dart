import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:pawiis/controllers/base_controller.dart';
import 'package:pawiis/models/user.dart';
import 'package:pawiis/repositories/user_repositoy.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashController extends BaseController {
  bool _isLogin = false;

  void checkUserLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('login') != null) {
      setState(() {
        _isLogin = prefs.getBool('login');
      });
    }
    if (_isLogin) {
      currentUser.value = User.fromJson(json.decode(prefs.getString('user')));
      Navigator.pushReplacementNamed(context, '/Home');
    } else {
      new Future.delayed(const Duration(seconds: 2), () {
        Navigator.pushReplacementNamed(context, '/Login');
      });
    }
  }
}
