import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pawiis/controllers/base_controller.dart';
import 'package:pawiis/dialogs/dialog.dart';
import 'package:pawiis/models/pet.dart';
import 'package:pawiis/repositories/pet_repository.dart';
import 'package:pawiis/repositories/user_repositoy.dart';
import 'package:pawiis/services/validations.dart';

class PetController extends BaseController {
  bool isReadOnly = true;
  bool isNew = true;
  File image;
  Validations validations;
  List<String> petTypes;
  Pet pet;
  TextEditingController petNameController;
  TextEditingController ownerNameController;

  PetController() {
    validations = new Validations();
    petTypes = new List();
    petTypes.add('Canino');
    pet = new Pet();
    ownerNameController = new TextEditingController();
    petNameController = new TextEditingController();
  }

  void handleFormSubmitted() {
    //showProgressDialog();
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true;
      //cancelProgressDialog();
      showInSnackBar('Por favor corrija los errores primero');
    } else {
      form.save();
      doPetRegister();
    }
  }

  doPetRegister() async {
    if (pet.petImage.isNotEmpty) {
      showProgressDialog();
      pet.petOwnerId = currentUser.value.id.toString();
      pet.ownerName = currentUser.value.name;
      //pet.createdAt = new DateTime.now();
      //pet.updatedAt = new DateTime.now();
      registerPet(pet).then((value) async {
        cancelProgressDialog();
        if (await MyDialog.showNativePopUpWithButton(context, 'Información',
            'Su mascota fue registrada correctamente', 'Aceptar')) {
          Navigator.of(context)
              .pushNamedAndRemoveUntil(
              '/Home', (Route<dynamic> route) => false);
        }
      }).catchError((onError) {
        cancelProgressDialog();
        showInSnackBar('Algo salio mal');
      });
    } else {
      showInSnackBar('Debe cargar una foto');
    }
  }

  void choseImageBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.camera_alt),
                    title: new Text('Camara'),
                    onTap: () {
                      chooseNewImage(ImageSource.camera);
                      Navigator.pop(context);
                    }),
                new ListTile(
                  leading: new Icon(Icons.toys),
                  title: new Text('Galeria'),
                  onTap: () {
                    chooseNewImage(ImageSource.gallery);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        });
  }

  Future chooseNewImage(var providerType) async {
    await ImagePicker.pickImage(source: providerType, imageQuality: 60)
        .then((image) {
      setState(() {
        if (image != null) {
          this.image = image;
          pet.petImage = image.readAsBytesSync();
        }
      });
    });
  }

  removePet() async {
    if (await MyDialog.showNativePopUpWith2Buttons(context, 'Importante',
        'Desea eliminar esta mascota', 'Cancelar', 'Borrar')) {
      showProgressDialog();
      deletePet(pet).then((value) async {
        cancelProgressDialog();
        if (await MyDialog.showNativePopUpWithButton(context, 'Información',
            'Mascota fue eliminada correctamente', 'Aceptar')) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/Home', (Route<dynamic> route) => false);
        }
      }).catchError((onError) {
        cancelProgressDialog();
        showInSnackBar('Algo salio mal');
      });
    }
  }

  handleUpdateFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true;
      showInSnackBar('Por favor corrija los errores primero');
    } else {
      form.save();
      update();
    }
  }

  update() {
    showProgressDialog();
    updatePet(pet).then((value) async {
      cancelProgressDialog();
      if (await MyDialog.showNativePopUpWithButton(context, 'Información',
          'Mascota fue actualizada correctamente', 'Aceptar')) {
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/Home', (Route<dynamic> route) => false);
      }
    }).catchError((onError) {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal' );
    });
  }
}
