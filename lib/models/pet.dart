import 'dart:convert';
import 'dart:io';

import 'dart:typed_data';

class Pet{
  int id;
  String petOwnerId;
  String petName;
  String petType;
  DateTime createdAt;
  DateTime updatedAt;
  String ownerName;
  Uint8List petImage;
  String avatarPath;

  Pet({this.ownerName, this.petType, this.updatedAt, this.createdAt, this.petOwnerId, this.petName, this.id, this.petImage});

  toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pet_name'] = this.petName;
    data['pet_owner_name'] = this.ownerName;
    data['name'] = this.ownerName;
    data['pet_type'] = this.petType;
    data['pet_image'] = this.petImage != null ? base64Encode(this.petImage) : null;
    data['pet_owner_id'] = this.petOwnerId;
    data['avatar'] = this.avatarPath;
    //data['createdAt'] = this.createdAt;
    //data['updateAt'] = this.updatedAt;
    data['id'] = this.id;
    return data;
  }

  Pet.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    petName = json['pet_name'];
    petType = json['pet_type'];
    petOwnerId = json['pet_owner_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }
}