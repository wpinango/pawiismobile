class User {
  int id;
  String email;
  String password;
  String name;
  String lastName;
  String token;
  String createdAt;
  String updatedAt;

  User(
      {this.token,
      this.email,
      this.createdAt,
      this.id,
      this.password,
      this.updatedAt,
      this.name, this.lastName});

  toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['name'] = this.name;
    data['last_name'] = this.lastName;
    data['token'] = this.token;
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updatedAt;
    data['id'] = this.id;
    return data;
  }

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    lastName = json['last_name'];
    email = json['email'];
    token = json['token'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }
}
