class RouteArgument {
  String id;
  String heroTag;
  String calledFrom;
  dynamic param;

  RouteArgument({this.id, this.heroTag, this.param, this.calledFrom,});

  @override
  String toString() {
    return '{id: $id, heroTag:${heroTag.toString()}}';
  }
}