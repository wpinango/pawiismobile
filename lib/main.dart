import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pawiis/app_theme.dart';
import 'package:pawiis/routes_generator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  static String url = 'http://192.168.0.110:8000/';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Pawiis',
        initialRoute: '/Splash',
        onGenerateRoute: RouteGenerator.generateRoute,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: AppTheme.mainColor,
        )
    );
  }
}
