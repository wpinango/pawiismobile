import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pawiis/pages/home_screen.dart';
import 'package:pawiis/pages/login_screen.dart';
import 'package:pawiis/pages/pet_screen.dart';
import 'package:pawiis/pages/register_pet_screen.dart';
import 'package:pawiis/pages/splash_screen.dart';

import 'models/route_argument.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => FirstScreen());
      case '/Login':
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case '/Home':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case '/RegisterPet':
        return MaterialPageRoute(builder: (_) => RegisterPetScreen());
      case '/PetInfo':
        return MaterialPageRoute(builder: (_) => PetScreen(routeArgument: args as RouteArgument));
    }
  }
}