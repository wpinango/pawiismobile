import 'dart:ui';

class AppTheme {
  static Color mainColor = Color(0xffE96723);
  static Color mainDarkColor = Color(0xff371F67);
  static Color secondColor = Color(0xffffffff);
  static Color secondDarkColor = Color(0xffffffff);
  static Color accentColor = Color(0xff5e35b1);
  static Color accentDarkColor = Color(0xff371F67);
  static Color scaffoldDarkColor =  Color(0xff371F67);
  static Color scaffoldColor = Color(0xff5e35b1);
  static Color hintColor = Color(0xff5e35b1);
  static Color borderColor = Color(0xffe8ecef);
  static Color iconColor = Color(0xff5e35b1);
  static Color inputBackColor = Color(0xffF3F3F3);
  static Color backgroundColor = Color(0xffFAFBFD);
  static String appVersion = '1.0';

  static TextStyle buttonTextStyle = TextStyle(color: mainColor);
  static TextStyle hintStyle = TextStyle(color: mainColor);
  static TextStyle textStyle = TextStyle(color: mainColor);
}