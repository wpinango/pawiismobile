import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:pawiis/controllers/pet_controller.dart';
import 'package:pawiis/models/route_argument.dart';

import '../app_theme.dart';

class PetScreen extends StatefulWidget {
  final RouteArgument routeArgument;

  const PetScreen({Key key, this.routeArgument}) : super(key: key);

  @override
  _PetScreenState createState() => new _PetScreenState();
}

class _PetScreenState extends StateMVC<PetScreen> {
  PetController _con;

  _PetScreenState() : super(PetController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.pet = widget.routeArgument.param;
    _con.ownerNameController.text = _con.pet.ownerName;
    _con.petNameController.text = _con.pet.petName;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Mi mascota',
          style: new TextStyle(
            //color: AppTheme.mainColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[
          _con.isReadOnly ?
          IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {
                setState(() {
                  _con.isReadOnly = false;
                });
              }) :
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                setState(() {
                  _con.removePet();
                });
              })
        ],
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
            child: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Form(
              key: _con.formKey,
              child: Column(children: <Widget>[
                SizedBox(
                  height: 16,
                ),
                Center(
                  child: Container(
                    width: 140.0,
                    height: 140.0,
                    child: !_con.isReadOnly
                        ? BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(80.0),
                                color: Colors.grey.withOpacity(0.6),
                              ),
                              child: IconButton(
                                onPressed: () {
                                  _con.choseImageBottomSheet();
                                },
                                icon: Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                  size: MediaQuery.of(context).size.width / 5,
                                ),
                              ),
                            ),
                          )
                        : null,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: _con.image != null
                            ? FileImage(_con.image)
                            : _con.pet.avatarPath != ''
                            ? NetworkImage(
                            _con.pet.avatarPath)
                            : AssetImage('assets/profile.png'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(80.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                ListTile(
                  title: Text('Numero de registro'),
                  subtitle: Text(_con.pet.id.toString()),
                ),
                !_con.isReadOnly
                    ? TextFormField(
                        decoration: InputDecoration(labelText: 'Nombre'),
                        validator: _con.validations.validateTextInput,
                        controller: _con.petNameController,
                        keyboardType: TextInputType.emailAddress,
                        readOnly: _con.isReadOnly,
                        onSaved: (String name) {
                          _con.pet.petName = name;
                        },
                      )
                    : ListTile(
                        title: Text('Nombre'),
                        subtitle: Text(_con.pet.petName),
                      ),
                !_con.isReadOnly
                    ? TextFormField(
                        decoration: InputDecoration(labelText: 'Dueño'),
                        validator: _con.validations.validateTextInput,
                        keyboardType: TextInputType.emailAddress,
                        controller: _con.ownerNameController,
                        readOnly: true,
                        onSaved: (String name) {
                          _con.pet.ownerName = name;
                        },
                      )
                    : ListTile(
                        title: Text('Dueño'),
                        subtitle: Text(_con.pet.ownerName),
                      ),
                !_con.isReadOnly
                    ? DropdownButtonFormField<String>(
                        validator: _con.validations.validateTextInput,
                        decoration:
                            InputDecoration(labelText: 'Tipo de Mascota'),
                        value: _con.pet.petType,
                        items: _con.petTypes.map((petType) {
                          return DropdownMenuItem(
                            value: petType,
                            child: Text(petType),
                          );
                        }).toList(),
                        onChanged: (val) {
                          setState(() {
                            _con.pet.petType = val;
                          });
                        },
                      )
                    : ListTile(
                        title: Text('Mascota'),
                        subtitle: Text(_con.pet.petType),
                      ),
                SizedBox(
                  height: 24,
                ),
                !_con.isReadOnly
                    ? InkWell(
                        onTap: () {
                          _con.handleUpdateFormSubmitted();
                        },
                        child: new Container(
                          height: 50,
                          alignment: FractionalOffset.center,
                          decoration: new BoxDecoration(
                              color: AppTheme.mainColor,
                              borderRadius: new BorderRadius.all(
                                  const Radius.circular(25.0)),
                              border: new Border.all(
                                color: const Color.fromRGBO(221, 221, 221, 1.0),
                              )),
                          child: new Text(
                            'Actualizar',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 32,
                )
              ])),
        )),
        inAsyncCall: _con.isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }
}
