import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:pawiis/app_theme.dart';
import 'package:pawiis/controllers/user_controller.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends StateMVC<LoginScreen> {
  UserController _con;

  _LoginScreenState() : super(UserController()) {
    _con = controller;
  }

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: _con.scaffoldKey,
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          child: new Container(
            height: screenSize.height,
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  'assets/logo.png',
                  height: screenSize.width / 5,
                ),
                Container(
                  margin: EdgeInsets.only(left: 24, right: 24),
                  padding: EdgeInsets.all(8),
                  child: new Form(
                    key: _con.formKey,
                    autovalidate: _con.autoValidate,
                    child: new Column(
                      children: <Widget>[
                        SizedBox(
                          height: 16,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Email'),
                          validator: _con.validations.validateTextInput,
                          keyboardType: TextInputType.emailAddress,
                          onSaved: (String email) {
                            _con.loginUser.email = email;
                          },
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            suffixIcon: GestureDetector(
                              onTap: () {
                                setState(() {
                                  _con.hidePass = !_con.hidePass;
                                });
                              },
                              child: Icon(
                                !_con.hidePass
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                            ),
                          ),
                          keyboardType: TextInputType.text,
                          obscureText: _con.hidePass,
                          validator: _con.validations.validateTextInput,
                          onSaved: (String pass) {
                            _con.loginUser.password = pass;
                          },
                        ),
                        SizedBox(
                          height: 32,
                        ),
                        InkWell(
                          onTap: () {
                            _con.handleFormSubmitted();
                          },
                          child: new Container(
                            height: 50,
                            alignment: FractionalOffset.center,
                            decoration: new BoxDecoration(
                                color: AppTheme.mainColor,
                                borderRadius: new BorderRadius.all(
                                    const Radius.circular(25.0)),
                                border: new Border.all(
                                  color:
                                      const Color.fromRGBO(221, 221, 221, 1.0),
                                )),
                            child: new Text(
                              'Entrar',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        inAsyncCall: _con.isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }
}