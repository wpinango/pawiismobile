import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:pawiis/controllers/pet_controller.dart';
import 'package:pawiis/models/route_argument.dart';

import '../app_theme.dart';

class RegisterPetScreen extends StatefulWidget {
  final RouteArgument routeArgument;

  const RegisterPetScreen({Key key, this.routeArgument}) : super(key: key);

  @override
  _RegisterPetScreenState createState() => new _RegisterPetScreenState();
}

class _RegisterPetScreenState extends StateMVC<RegisterPetScreen> {
  PetController _con;

  _RegisterPetScreenState() : super(PetController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Registrar mascota',
          style: new TextStyle(
            //color: AppTheme.mainColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
            child: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Form(
              key: _con.formKey,
              child: Column(children: <Widget>[
                SizedBox(height: 16,),
                Center(
                  child: Container(
                    width: 140.0,
                    height: 140.0,
                    child: _con.isNew
                        ? BackdropFilter(
                      filter:
                      ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(80.0),
                          color: Colors.grey.withOpacity(0.6),
                        ),
                        child: IconButton(
                          onPressed: () {
                            _con.choseImageBottomSheet();
                          },
                          icon: Icon(
                            Icons.edit,
                            color: Colors.white,
                            size:
                            MediaQuery.of(context).size.width /
                                5,
                          ),
                        ),
                      ),
                    )
                        : null,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: _con.image != null
                            ? FileImage(_con.image)
                            : AssetImage('assets/avatar.png'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(80.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Nombre'),
                  validator: _con.validations.validateTextInput,
                  keyboardType: TextInputType.emailAddress,
                  textCapitalization: TextCapitalization.sentences,
                  onSaved: (String name) {
                    _con.pet.petName = name;
                  },
                ),
                DropdownButtonFormField<String>(
                  validator: _con.validations.validateTextInput,
                  decoration: InputDecoration(labelText: 'Tipo de Mascota'),
                  value: _con.pet.petType,
                  items: _con.petTypes.map((petType) {
                    return DropdownMenuItem(
                      value: petType,
                      child: Text(petType),
                    );
                  }).toList(),
                  onChanged: (val) {
                    setState(() {
                      _con.pet.petType = val;
                    });
                  },
                ),
                SizedBox(height: 24,),
                InkWell(
                  onTap: () {
                    _con.handleFormSubmitted();
                  },
                  child: new Container(
                    height: 50,
                    alignment: FractionalOffset.center,
                    decoration: new BoxDecoration(
                        color: AppTheme.mainColor,
                        borderRadius:
                            new BorderRadius.all(const Radius.circular(25.0)),
                        border: new Border.all(
                          color: const Color.fromRGBO(221, 221, 221, 1.0),
                        )),
                    child: new Text(
                      'Registrar',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ])),
        )),
        inAsyncCall: _con.isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }
}
