import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:pawiis/components/drawer_widget.dart';
import 'package:pawiis/components/pets_item_widget.dart';
import 'package:pawiis/controllers/home_controller.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends StateMVC<HomeScreen> {
  HomeController _con;

  _HomeScreenState() : super(HomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getMyPets();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text('Pawiis'),
        actions: [
          IconButton(icon: Icon(Icons.refresh), onPressed: () {_con.getMyPets();})
        ],
      ),
      drawer: DrawerWidget(
        drawerItems: _con.drawerItems,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/RegisterPet');
        },
        child: Icon(Icons.add),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _con.isInAsyncCall,
        child: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: ListView.builder(
              itemCount: _con.pets.length,
              itemBuilder: (BuildContext c, int index) {
                return PetItemWidget(
                  pet: _con.pets[index],
                  key: Key(_con.pets[index].id.toString()),
                );
              }),
        ),
      ),
    );
  }
}
